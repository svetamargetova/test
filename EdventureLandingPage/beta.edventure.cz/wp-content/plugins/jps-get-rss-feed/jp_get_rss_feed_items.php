<?php
/*
Plugin Name: JP's Get RSS Feed
Plugin URI: http://www.jimmyscode.com/wordpress/jps-get-rss-feeds/
Description: Get last X number of posts from selected RSS feed and display in an unordered list. Default is 5 items.
Version: 1.3.9
Author: Jimmy Pe&ntilde;a
Author URI: http://www.jimmyscode.com/
License: GPLv2 or later
*/
// plugin constants
define('JPGRF_VERSION', '1.3.9');
define('JPGRF_PLUGIN_NAME', 'JP\'s Get RSS Feed');
define('JPGRF_SLUG', 'jps-get-rss-feed');
define('JPGRF_LOCAL', 'jp_get_rss_feed');
define('JPGRF_OPTION', 'jp_get_rss_feed');
/* default values */
define('JPGRF_DEFAULT_ENABLED', true);
define('JPGRF_DEFAULT_URL', '');
define('JPGRF_DEFAULT_NUM', 5);
define('JPGRF_DEFAULT_NOFOLLOW', true);
define('JPGRF_DEFAULT_CSSCLASS', 'jpgetrssfeed');
define('JPGRF_DEFAULT_DESC', false);
define('JPGRF_DEFAULT_SHOW', false);
define('JPGRF_DEFAULT_NEWWINDOW', false);
/* option array member names */
define('JPGRF_DEFAULT_ENABLED_NAME', 'enabled');
define('JPGRF_DEFAULT_URL_NAME', 'url');
define('JPGRF_DEFAULT_NUM_NAME', 'numitems');
define('JPGRF_DEFAULT_NOFOLLOW_NAME', 'nofollow');
define('JPGRF_DEFAULT_CSSCLASS_NAME', 'cssclass');
define('JPGRF_DEFAULT_DESC_NAME', 'getdesc');
define('JPGRF_DEFAULT_SHOW_NAME', 'show');
define('JPGRF_DEFAULT_NEWWINDOW_NAME', 'opennewwindow');

// add custom quicktag
add_action('admin_print_footer_scripts', 'add_jpgrf_quicktag', 100);
function add_jpgrf_quicktag() {
?>
<script>
QTags.addButton('jpgrf', 'RSS Feed', '[jp-rss-feed]', '', '', 'JP\'s Get RSS Feed', '' );
</script>
<?php }

// localization to allow for translations
add_action('init', 'jp_get_rss_feed_translation_file');
function jp_get_rss_feed_translation_file() {
  $plugin_path = plugin_basename(dirname(__FILE__) . '/translations');
  load_plugin_textdomain(JPGRF_LOCAL, '', $plugin_path);
}
// tell WP that we are going to use new options
// also, register the admin CSS file for later inclusion
add_action('admin_init', 'jp_get_rss_feed_options_init');
function jp_get_rss_feed_options_init() {
  register_setting('jp_get_rss_feed_options', JPGRF_OPTION, 'jpgrf_validation');
  register_jpgrf_admin_style();
}
// validation function
function jpgrf_validation($input) {
  // sanitize url
  $input[JPGRF_DEFAULT_URL_NAME] = esc_url($input[JPGRF_DEFAULT_URL_NAME]);
  // sanitize number of items
  $input[JPGRF_DEFAULT_NUM_NAME] = intval($input[JPGRF_DEFAULT_NUM_NAME]);
  if ($input[JPGRF_DEFAULT_NUM_NAME] == 0) { // not an integer, set to default
    $input[JPGRF_DEFAULT_NUM_NAME] = JPGRF_DEFAULT_NUM;
  }
  // sanitize css class
  $input[JPGRF_DEFAULT_CSSCLASS_NAME] = sanitize_html_class($input[JPGRF_DEFAULT_CSSCLASS_NAME]);
	if (!$input[JPGRF_DEFAULT_CSSCLASS_NAME]) {
		$input[JPGRF_DEFAULT_CSSCLASS_NAME] = JPGRF_DEFAULT_CSSCLASS;
	}
  return $input;
}
// add Settings sub-menu
add_action('admin_menu', 'jpgrf_plugin_menu');
function jpgrf_plugin_menu() {
  add_options_page(JPGRF_PLUGIN_NAME, JPGRF_PLUGIN_NAME, 'manage_options', JPGRF_SLUG, 'jp_get_rss_feed_page');
}
// plugin settings page
// http://planetozh.com/blog/2009/05/handling-plugins-options-in-wordpress-28-with-register_setting/
function jp_get_rss_feed_page() {
  // check perms
  if (!current_user_can('manage_options')) {
    wp_die(__('You do not have sufficient permission to access this page', JPGRF_LOCAL));
  }
  ?>
  <div class="wrap">
    <?php screen_icon(); ?>
    <h2><?php echo JPGRF_PLUGIN_NAME; ?></h2>
    <form method="post" action="options.php">
	<?php submit_button(); ?>
      <?php settings_fields('jp_get_rss_feed_options'); ?>
      <?php $options = jpgrf_getpluginoptions(); ?>
      <?php update_option(JPGRF_OPTION, $options); ?>
      <table class="form-table" id="theme-options-wrap">
        <tr valign="top"><th scope="row"><strong><label title="<?php _e('Is plugin enabled? Uncheck this to turn it off temporarily.', JPGRF_LOCAL); ?>" for="jp_get_rss_feed[<?php echo JPGRF_DEFAULT_ENABLED_NAME; ?>]"><?php _e('Plugin enabled?', JPGRF_LOCAL); ?></label></strong></th>
          <td><input type="checkbox" id="jp_get_rss_feed[<?php echo JPGRF_DEFAULT_ENABLED_NAME; ?>]" name="jp_get_rss_feed[<?php echo JPGRF_DEFAULT_ENABLED_NAME; ?>]" value="1" <?php checked('1', $options[JPGRF_DEFAULT_ENABLED_NAME]); ?> /></td>
        </tr>
    	  <tr valign="top"><td colspan="2"><?php _e('Is plugin enabled? Uncheck this to turn it off temporarily.', JPGRF_LOCAL); ?></td></tr>
        <tr valign="top"><th scope="row"><strong><label title="<?php _e('Enter the default URL here. This will be used wherever you use the shortcode, if you do not specify the URL.', JPGRF_LOCAL); ?>" for="jp_get_rss_feed[<?php echo esc_url(JPGRF_DEFAULT_URL_NAME); ?>]"><?php _e('Default URL for feeds', JPGRF_LOCAL); ?></label></strong></th>
          <td><input type="url" id="jp_get_rss_feed[<?php echo JPGRF_DEFAULT_URL_NAME; ?>]" name="jp_get_rss_feed[<?php echo JPGRF_DEFAULT_URL_NAME; ?>]" value="<?php echo $options[JPGRF_DEFAULT_URL_NAME]; ?>" /></td>
        </tr>
        <tr valign="top"><td colspan="2"><?php _e('Enter the default URL here. This will be used wherever you use the shortcode, if you do not specify the URL.', JPGRF_LOCAL); ?></td></tr>
        <tr valign="top"><th scope="row"><strong><label title="<?php _e('Enter the default number of RSS feed items to display, if you do not pass a value to the plugin.', JPGRF_LOCAL); ?>" for="jp_get_rss_feed[<?php echo JPGRF_DEFAULT_NUM_NAME; ?>]"><?php _e('Default number of items', JPGRF_LOCAL); ?></label></strong></th>
          <td><input type="number" min="1" id="jp_get_rss_feed[<?php echo JPGRF_DEFAULT_NUM_NAME; ?>]" name="jp_get_rss_feed[<?php echo JPGRF_DEFAULT_NUM_NAME; ?>]" value="<?php echo $options[JPGRF_DEFAULT_NUM_NAME]; ?>" /></td>
        </tr>
        <tr valign="top"><td colspan="2"><?php _e('Enter the default number of RSS feed items to display, if you do not pass a value to the plugin.', JPGRF_LOCAL); ?></td></tr>
        <tr valign="top"><th scope="row"><strong><label title="<?php _e('Do you want to add rel=nofollow to all links?', JPGRF_LOCAL); ?>" for="jp_get_rss_feed[<?php echo JPGRF_DEFAULT_NOFOLLOW_NAME; ?>]"><?php _e('Nofollow links?', JPGRF_LOCAL); ?></label></strong></th>
          <td><input type="checkbox" id="jp_get_rss_feed[<?php echo JPGRF_DEFAULT_NOFOLLOW_NAME; ?>]" name="jp_get_rss_feed[<?php echo JPGRF_DEFAULT_NOFOLLOW_NAME; ?>]" value="1" <?php checked('1', $options[JPGRF_DEFAULT_NOFOLLOW_NAME]); ?> /></td>
        </tr>
        <tr valign="top"><td colspan="2"><?php _e('Do you want to add rel="nofollow" to all links?', JPGRF_LOCAL); ?></td></tr>
        <tr valign="top"><th scope="row"><strong><label title="<?php _e('Enter the default CSS class name, if you do not pass a value to the plugin.', JPGRF_LOCAL); ?>" for="jp_get_rss_feed[<?php echo JPGRF_DEFAULT_CSSCLASS_NAME; ?>]"><?php _e('Default CSS class', JPGRF_LOCAL); ?></label></strong></th>
          <td><input type="text" id="jp_get_rss_feed[<?php echo JPGRF_DEFAULT_CSSCLASS_NAME; ?>]" name="jp_get_rss_feed[<?php echo JPGRF_DEFAULT_CSSCLASS_NAME; ?>]" value="<?php echo $options[JPGRF_DEFAULT_CSSCLASS_NAME]; ?>" /></td>
        </tr>
        <tr valign="top"><td colspan="2"><?php _e('Enter the default CSS class name, if you do not pass a value to the plugin.', JPGRF_LOCAL); ?></td></tr>
        <tr valign="top"><th scope="row"><strong><label title="<?php _e('Check this box to show the feed item description after the link.', JPGRF_LOCAL); ?>" for="jp_get_rss_feed[<?php echo JPGRF_DEFAULT_DESC_NAME; ?>]"><?php _e('Include feed item description?', JPGRF_LOCAL); ?></label></strong></th>
          <td><input type="checkbox" id="jp_get_rss_feed[<?php echo JPGRF_DEFAULT_DESC_NAME; ?>]" name="jp_get_rss_feed[<?php echo JPGRF_DEFAULT_DESC_NAME; ?>]" value="1" <?php checked('1', $options[JPGRF_DEFAULT_DESC_NAME]); ?> /></td>
        </tr>
    	  <tr valign="top"><td colspan="2"><?php _e('Check this box to show the feed item description after the link.', JPGRF_LOCAL); ?></td></tr>				
        <tr valign="top"><th scope="row"><strong><label title="<?php _e('Check this box to open links in a new window. target=_blank will be added to all links', JPGRF_LOCAL); ?>" for="jp_get_rss_feed[<?php echo JPGRF_DEFAULT_NEWWINDOW_NAME; ?>]"><?php _e('Open links in new window?', JPGRF_LOCAL); ?></label></strong></th>
          <td><input type="checkbox" id="jp_get_rss_feed[<?php echo JPGRF_DEFAULT_NEWWINDOW_NAME; ?>]" name="jp_get_rss_feed[<?php echo JPGRF_DEFAULT_NEWWINDOW_NAME; ?>]" value="1" <?php checked('1', $options[JPGRF_DEFAULT_NEWWINDOW_NAME]); ?> /></td>
        </tr>
    	  <tr valign="top"><td colspan="2"><?php _e('Check this box to open links in a new window. target="_blank" will be added to all links', JPGRF_LOCAL); ?></td></tr>				
      </table>
	<?php submit_button(); ?>
    </form>
    <h3>Plugin Arguments and Defaults</h3>
    <table class="widefat">
      <thead>
        <tr>
          <th>Argument</th>
          <th>Type</th>
          <th>Default Value</th>
        </tr>
      </thead>
      <tbody>
    <?php $plugin_defaults = jpgrf_shortcode_defaults(); foreach($plugin_defaults as $key => $value) { ?>
        <tr>
          <td><?php echo $key; ?></td>
					<td><?php echo gettype($value); ?></td>
          <td> <?php 
						if ($value === true) {
							echo 'true';
						} elseif ($value === false) {
							echo 'false';
						} elseif ($value === '') {
							echo '<em>(this value is blank by default)</em>';
						} else {
							echo $value;
						} ?></td>
        </tr>
    <?php } ?>
    </tbody>
    </table>
    <?php screen_icon('edit-comments'); ?>
    <h3>Support</h3>
			<div class="support">
      If you like this plugin, please <a href="http://wordpress.org/support/view/plugin-reviews/<?php echo JPGRF_SLUG; ?>/">rate it on WordPress.org</a> and click the "Works" button so others know it will work for your WordPress version. For support please visit the <a href="http://wordpress.org/support/plugin/<?php echo JPGRF_SLUG; ?>">forums</a>. <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=7EX9NB9TLFHVW"><img src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif" alt="Donate with PayPal" title="Donate with PayPal" width="92" height="26" /></a>
      </div>
  </div>
  <?php }
// shortcode for posts and pages
add_shortcode('jp-rss-feed', 'jp_get_rss_feed_items');
// one function for shortcode and PHP
function jp_get_rss_feed_items($atts) {
  // get parameters
  extract(shortcode_atts(jpgrf_shortcode_defaults(), $atts));
  // plugin is enabled/disabled from settings page only
  $options = jpgrf_getpluginoptions();
  $enabled = $options[JPGRF_DEFAULT_ENABLED_NAME];
  
  // ******************************
  // derive shortcode values from constants
  // ******************************
  $temp_url = constant('JPGRF_DEFAULT_URL_NAME');
  $feed_url = $$temp_url;
  $temp_number = constant('JPGRF_DEFAULT_NUM_NAME');
  $number_of_items = $$temp_number;
  $temp_nofollow = constant('JPGRF_DEFAULT_NOFOLLOW_NAME');
  $nofollow = $$temp_nofollow;
  $temp_cssclass = constant('JPGRF_DEFAULT_CSSCLASS_NAME');
  $cssclass = $$temp_cssclass;
  $temp_desc = constant('JPGRF_DEFAULT_DESC_NAME');
  $desc = $$temp_desc;
  $temp_window = constant('JPGRF_DEFAULT_NEWWINDOW_NAME');
  $opennewwindow = $$temp_window;
  $temp_show = constant('JPGRF_DEFAULT_SHOW_NAME');
  $show = $$temp_show;

  // ******************************
  // sanitize user input
  // ******************************
  $feed_url = esc_url($feed_url);
  $number_of_items = intval($number_of_items);
  if (!$number_of_items) {
    $number_of_items = JPGRF_DEFAULT_NUM;
  }
  $nofollow = (bool)$nofollow;
  $cssclass = sanitize_html_class($cssclass);
  if (!$cssclass) {
    $cssclass = JPGRF_DEFAULT_CSSCLASS;
  }
  $desc = (bool)$desc;
  $opennewwindow = (bool)$opennewwindow;
  $show = (bool)$show;

  // ******************************
  // check for parameters, then settings, then defaults
  // ******************************
  if ($enabled) {
    // check for overridden parameters, if nonexistent then get from DB
    if ($feed_url === JPGRF_DEFAULT_URL) { // no url passed to function, try settings page
      $feed_url = $options[JPGRF_DEFAULT_URL_NAME];
      if (($feed_url === JPGRF_DEFAULT_URL) || ($feed_url === false)) { // no url on settings page either
        $enabled = false;
      }
    }
    if ($number_of_items == JPGRF_DEFAULT_NUM) { // nothing passed to plugin, check settings page
      $number_of_items = $options[JPGRF_DEFAULT_NUM_NAME];
      if ($number_of_items === false) { // nothing on settings page
        $number_of_items = JPGRF_DEFAULT_NUM;
      }
    }
    if ($nofollow === JPGRF_DEFAULT_NOFOLLOW) {
      $nofollow = $options[JPGRF_DEFAULT_NOFOLLOW_NAME];
      if ($nofollow === false) {
        $nofollow = JPGRF_DEFAULT_NOFOLLOW;
      }
    }
    if ($cssclass == JPGRF_DEFAULT_CSSCLASS) {
      $cssclass = $options[JPGRF_DEFAULT_CSSCLASS_NAME];
      if ($cssclass === false) {
        $cssclass = JPGRF_DEFAULT_CSSCLASS;
      }
    }
    if ($desc === JPGRF_DEFAULT_DESC) {
			$desc = $options[JPGRF_DEFAULT_DESC_NAME];
			if ($desc === false) {
				$desc = JPGRF_DEFAULT_DESC;
			}
    }
    if ($opennewwindow === JPGRF_DEFAULT_NEWWINDOW) {
			$opennewwindow = $options[JPGRF_DEFAULT_NEWWINDOW_NAME];
			if ($opennewwindow === false) {
				$opennewwindow = JPGRF_DEFAULT_NEWWINDOW;
			}
    }
  } // end enabled check

  // ******************************
  // do some actual work
  // ******************************
  if ($enabled) {
    include_once(ABSPATH . WPINC . '/feed.php');
    $rss = fetch_feed($feed_url);
    if (!is_wp_error($rss)) { // no error occurred
			$maxitems = $rss->get_item_quantity($number_of_items);
			// Build an array of all the items, starting with element 0 (first element).
			$rss_items = $rss->get_items(0, $maxitems);
      $output = '<div class="' . $cssclass . '"><ul>';
      if ($maxitems == 0) { // no items
        $output .= '<li>' . __('No feed items at this time. Please check back later.', JPGRF_LOCAL) . '</li>';
      } else { // there were items
        // Loop through each feed item and display each item as a hyperlink
				foreach ($rss_items as $item) {
					$output .= '<li>';
					$output .= '<a' . ($opennewwindow ? ' target="_blank" ' : ' ') . ($nofollow ? ' rel="nofollow" ' : ' ') . 'href="' . esc_url($item->get_permalink());
					$output .= '" title="' .  __('Posted', JPGRF_LOCAL) . ' ' . $item->get_date('j F Y | g:i a') . '">' . esc_html($item->get_title()) . '</a>' . ($desc ? ' - ' . $item->get_description() : '') . '</li>';
				} // end foreach
      } // end items check
			$output .= '</ul></div>';
    } // end error check
  } else { // plugin disabled
    $output = '<!-- ' . JPGRF_PLUGIN_NAME . ': plugin is disabled. Either you did not pass a necessary setting to the plugin, or did not configure a default. Check Settings page. -->';
  } // end enabled check
  if ($enabled) {
    if ($show) {
      echo $output;
    } else {
      return $output;
    }  
  }
} // end shortcode function
// show admin messages to plugin user
add_action('admin_notices', 'jpgrf_showAdminMessages');
function jpgrf_showAdminMessages() {
  // http://wptheming.com/2011/08/admin-notices-in-wordpress/
  global $pagenow;
  if (current_user_can('manage_options')) { // user has privilege
    if ($pagenow == 'options-general.php') {
      if ($_GET['page'] == JPGRF_SLUG) { // we are on JP's Get RSS Feed settings page
        $options = jpgrf_getpluginoptions();
        if ($options != false) {
          $enabled = $options[JPGRF_DEFAULT_ENABLED_NAME];
          $feed_url = $options[JPGRF_DEFAULT_URL_NAME];
          if (!$enabled) {
            echo '<div id="message" class="error">' . JPGRF_PLUGIN_NAME . ' ' . __('is currently disabled.', JPGRF_LOCAL) . '</div>';
          }
          if (($feed_url === JPGRF_DEFAULT_URL) || ($feed_url === false)) {
            echo '<div id="message" class="updated">' . __('WARNING: No default URL specified. You will need to pass a URL to the plugin each time you use the shortcode or PHP function.', JPGRF_LOCAL) . '</div>';
          }
        }
      }
    } // end page check
  } // end privilege check
} // end admin msgs function
// add admin CSS if we are on the plugin options page
add_action('admin_head', 'insert_jpgrf_admin_css');
function insert_jpgrf_admin_css() {
  global $pagenow;
  if (current_user_can('manage_options')) { // user has privilege
    if ($pagenow == 'options-general.php') {
      if ($_GET['page'] == JPGRF_SLUG) { // we are on settings page
        jpgrf_admin_styles();
      }
    }
  }
}
// add settings link on plugin page
// http://bavotasan.com/2009/a-settings-link-for-your-wordpress-plugins/
add_filter('plugin_action_links_' . plugin_basename(__FILE__), 'jpgrf_plugin_settings_link');
function jpgrf_plugin_settings_link($links) { 
  $settings_link = '<a href="options-general.php?page=' . JPGRF_SLUG . '">' . __('Settings', JPGRF_LOCAL) . '</a>'; 
  array_unshift($links, $settings_link); 
  return $links; 
}
// enqueue/register the admin CSS file
function jpgrf_admin_styles() {
  wp_enqueue_style('jpgrf_admin_style');
}
function register_jpgrf_admin_style() {
  wp_register_style('jpgrf_admin_style', 
    plugins_url(plugin_basename(dirname(__FILE__)) . '/css/admin.css'), 
    array(), 
    JPGRF_VERSION, 
    'all');
}
// when plugin is activated, create options array and populate with defaults
register_activation_hook(__FILE__, 'jpgrf_activate');
function jpgrf_activate() {
  $options = jpgrf_getpluginoptions();
  update_option(JPGRF_OPTION, $options);
}
// generic function that returns plugin options from DB
// if option does not exist, returns plugin defaults
function jpgrf_getpluginoptions() {
  return get_option(JPGRF_OPTION, array(JPGRF_DEFAULT_ENABLED_NAME => JPGRF_DEFAULT_ENABLED, JPGRF_DEFAULT_URL_NAME => JPGRF_DEFAULT_URL, JPGRF_DEFAULT_NUM_NAME => JPGRF_DEFAULT_NUM, JPGRF_DEFAULT_NOFOLLOW_NAME => JPGRF_DEFAULT_NOFOLLOW, JPGRF_DEFAULT_CSSCLASS_NAME => JPGRF_DEFAULT_CSSCLASS, JPGRF_DEFAULT_DESC_NAME => JPGRF_DEFAULT_DESC, JPGRF_DEFAULT_NEWWINDOW_NAME => JPGRF_DEFAULT_NEWWINDOW));
}
// function to return shortcode defaults
function jpgrf_shortcode_defaults() {
  return array(
      JPGRF_DEFAULT_URL_NAME => JPGRF_DEFAULT_URL, 
      JPGRF_DEFAULT_NUM_NAME => JPGRF_DEFAULT_NUM, 
      JPGRF_DEFAULT_NOFOLLOW_NAME => JPGRF_DEFAULT_NOFOLLOW, 
      JPGRF_DEFAULT_CSSCLASS_NAME => JPGRF_DEFAULT_CSSCLASS, 
			JPGRF_DEFAULT_DESC_NAME => JPGRF_DEFAULT_DESC, 
      JPGRF_DEFAULT_NEWWINDOW_NAME => JPGRF_DEFAULT_NEWWINDOW, 
      JPGRF_DEFAULT_SHOW_NAME => JPGRF_DEFAULT_SHOW
  );
}
?>