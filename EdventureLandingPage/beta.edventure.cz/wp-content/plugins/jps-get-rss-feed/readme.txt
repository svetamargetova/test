=== JP's Get RSS Feed ===
Tags: rss, xml, feed, fetch, display, list
Requires at least: 3.5
Tested up to: 3.5.1
Contributors: jp2112
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=7EX9NB9TLFHVW
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Get last X number of posts from a selected RSS feed. Default is last 5 items. Includes shortcode for listing feed items on posts or pages.

== Description ==

<strong>Notice: Version 1.3.1 introduced admin menus for plugin settings. There is also a new way to pass parameters to the shortcode and PHP function. You should check all shortcodes and plugin function calls before and after upgrading, to confirm compatibility with this version.</strong>

= Features =

- Show RSS feed items on any post or page, in your sidebar or footer, or anywhere else using conditional tags in your PHP code
- Open links in a new window (optional)

This plugin uses WordPress' ability to return feeds, to get the last X number of items from any RSS feed. Display the last few items from any RSS feed of your choice. For example, your Twitter feed, or another blog or forum that outputs a RSS feed. Any RSS feed can be grabbed. Call it in your footer to list your last few tweets, or your sidebar to showcase content from another one of your blogs.

Uses `fetch_feed`, which was introduced in WordPress 2.8. Works and tested in WordPress 3.5. fetch_feed caches feeds for 12 hours.

Feed items are wrapped in a div tag, with class "jpgetrssfeed" so you can style the output in your CSS file. The items list is surrounded by `<ul></ul>` tags, with each feed item listed in a `<li></li>` tag. However, you can specify a new CSS class to style output differently for different feeds.

A button is added to the post editor toolbar so you can insert the shortcode in your posts or pages.

With help from: 
http://codex.wordpress.org/Function_Reference/fetch_feed

<strong>If you use and enjoy this plugin, please rate it and click the "Works" button below so others know that it works with the latest version of WordPress.</strong>

== Installation ==

1. Upload plugin file through the WordPress interface.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Go to Settings &raquo; JP's Get RSS Feed, configure plugin.
4. Insert shortcode on posts or pages, or use PHP function in functions.php or a plugin.

== Frequently Asked Questions ==

= How do I use the plugin? =

You can use the plugin in two ways:

1. In your PHP code (functions.php, or a plugin), you would call the plugin function like this:

`if (function_exists('jp_get_rss_feed_items')) {
  jp_get_rss_feed_items(array(
    'url' => "http://somefeed.com/rss", 
    'numitems' => 5,
    'nofollow' => true,
    'cssclass' => 'myclass',
    'getdesc' => false,
    'opennewwindow' => false,
    'show' => true    
  ));
}`

This will:
<ul>
<li>fetch the feed located at http://somefeed.com/rss</li>
<li>list the last 5 items</li>
<li>add rel="nofollow" to each link</li>
<li>add the "myclass" class to the div tag that wraps the output</li>
<li>hide the item description</li>
<li>open links in the same window</li>
<li>echo the content to the screen</li>
</ul>

This will override any settings you configured on the plugin's Settings page. Always wrap plugin function calls in a `function_exists` check so that your site doesn't go down if the plugin is inactive.

2. As a shortcode, you call the plugin like this:

[jp-rss-feed numitems="3"]

This will take the settings from the Settings page (or the defaults if a setting is missing) and apply them to the shortcode, except there will only be three items even if you specified 5 on the Settings page. In this way you can override settings by passing them to the shortcode at runtime.

= What are the plugin defaults? =

The following are the available parameters and their default values:

<ul>
<li>url => '' - the feed URL to parse</li>
<li>numitems => '5' - the number of items to fetch. Default is 5.</li>
<li>nofollow => true - include rel="nofollow" after each link? Default is to include it.</li>
<li>cssclass => 'jpgetrssfeed' - CSS class to style the output. Default is "jpgetrssfeed"</li>
<li>getdesc => false - include the feed item description in the output. Default is false (don't show).</li>
<li>opennewwindow => false - open links in the same browser window</li>
<li>show => false - whether to echo (true) or return (false) the output. Default is false (return the output)</li>
</ul>

You must enter these on the Settings page, or pass them to the shortcode/function as shown above. Leave out a parameter to use the default (from Settings page). If a value from the Settings page is unavailable, the default will be used (see above for defaults). However you must specify the URL somewhere, either on the plugin settings page or as a parameter passed to the shortcode or PHP function.

The order of precedence is:

parameters (1) -> Settings page (2) -> defaults (3)

i.e. If you do not pass a parameter, the value from the Settings page will be used. If there is no setting, the default will be used. However, if you do not specify a URL on the Settings page or as a parameter, the plugin will not function (it doesn't know what RSS feed to fetch!).

You must pass a URL to the plugin, either via parameter or as a value on the Settings page. Otherwise the plugin will simply do nothing.

The plugin arguments and default values may change over time. To get the latest list of arguments and defaults, look at the settings page.

= I want to use the plugin in a widget. How? =

Add this line of code to your functions.php:

`add_filter('widget_text', 'do_shortcode');`

Or, install a plugin to do it for you: http://blogs.wcnickerson.ca/wordpress/plugins/widgetshortcodes/

Now, add the built-in text widget that comes with WordPress, and insert the JP's Get RSS Feed shortcode into the text widget. See above for how to use the shortcode.

See http://digwp.com/2010/03/shortcodes-in-widgets/ for a detailed example.

Keep in mind, if you want to show your blog feed in a sidebar widget, WordPress already has a built-in "Recent Posts" widget for that.

= I don't want the post editor toolbar button. How do I remove it? =

Add this to your functions.php:

`remove_action('admin_print_footer_scripts', 'add_jpgrf_quicktag');`

= I inserted the shortcode but don't see anything on the page. =

Clear your browser cache and also clear your cache plugin (if any). If you still don't see anything, check your webpage source for the following:

`<!-- JP's Get RSS Feed: plugin is disabled. Check Settings page. -->`

This means you didn't pass a necessary setting to the plugin, so it disabled itself. You need to pass at least the URL, either by entering it on the settings page or passing it to the plugin in the shortcode or PHP function. You should also check that the "enabled" checkbox on the plugin settings page is checked. If that box is not checked, the plugin will do nothing even if you pass it a URL.

= Genesis Theme Framework users =

If you are using the Genesis framework from Studiopress, you might use the plugin like this:

`add_action('genesis_after_post_content', 'showrss');
function showrss() {
  if (is_page('faq')) { // we are on the FAQ page...
    if (function_exists('jp_get_rss_feed_items')) { // and the function exists...
      echo '<h3>Latest Articles From my Favorite RSS Feed</h3>';
      jp_get_rss_feed_items(array('url' => "http://feeds.feedburner.com/MyFavoriteRSSFeed", 'show' => true));
      // or: echo jp_get_rss_feed_items(array('url' => "http://feeds.feedburner.com/MyFavoriteRSSFeed"));
    }
  }
}`

This code would go in your functions.php file, or (ideally) in a plugin. Check the <a href="http://my.studiopress.com/docs/hook-reference/">Hook Reference</a> to determine where you want to place the output. The above example (remember, for Genesis framework only) would show the last five articles from a given RSS feed at the bottom of the 'FAQ' page.

= How can I list from multiple feeds? =

Use an array and a foreach loop:

`if (function_exists('jp_get_rss_feed_items')) {
  // create array
  $feedslist = array(
  "My feed URL number one",
  "My feed URL number two",
  "My feed URL number three"
  );
  // loop through array and call plugin
  foreach ($feedslist as $item) {
   jp_get_rss_feed_items(array('url' => $item, 'show' => true));
  }
}`

This will list the last five items from each feed in its own unordered list.

But suppose you want a different CSS class for each one. Use a for loop instead.

`if (function_exists('jp_get_rss_feed_items')) {
  // create array
  $feedslist = array(
  "My feed URL number one",
  "My feed URL number two",
  "My feed URL number three"
  );
  // loop through array and call plugin
  for ($i = 0, $size = count($feedslist); $i < $size; $i++) {
   jp_get_rss_feed_items(array('url' => $feedslist[$i], 'cssclass' => 'jpgetrssfeed_' . $i , 'show' => true));
  }
}`

So your CSS classes would be

- jpgetrssfeed_1
- jpgetrssfeed_2
- jpgetrssfeed_3

= How can I list items from a random feed? =

Use `array_rand`:

`if (function_exists('jp_get_rss_feed_items')) {
  // create array
  $feedslist = array(
  "My feed URL number one",
  "My feed URL number two",
  "My feed URL number three"
  );
  // get random index from array
  $item = array_rand($feedslist, 1);
  // pass randomly selected array member to plugin
  jp_get_rss_feed_items(array('url' => $feedslist[$item], 'show' => true));
}`

This selects one URL randomly and passes it to the plugin. 

= How can I style the output? =

Feed items are wrapped in a div tag, with class "jpgetrssfeed" (or whatever you change it to) so you can style the output in your CSS file. The items list is surrounded by `<ul></ul>` tags, with each feed item listed in a `<li></li>` tag.

So you could add something like this in your style.css:

`.jpgetrssfeed {border:1px solid gray;margin:10px 0}
.jpgetrssfeed ul li {list-style-type:circle}`

You can also specify your own class, or use a different class name for each shortcode to style it differently. Ex:

In my style.css file I add the following

.rssorange {border:1px solid #FF9900;margin:10px 0}
.rssblue {border:1px solid blue;margin:10px 0}
.rssred {border:1px solid red;margin:10px 0}

I specify each class in my shortcodes as follows:

[jp-rss-feed url="http://somefeed.com/rss" cssclass="rssorange"]
[jp-rss-feed url="http://some_other_feed.com/rss" cssclass="rssblue"]
[jp-rss-feed url="http://some_new_feed.com/rss" cssclass="rssred"]

Each feed will be surrounded by a different color border.

= How do I add a header or title above the feed items list? =

See the examples above. Before you call the shortcode or the PHP function, echo your header like this:

`echo '<h3>Latest Articles From my Favorite RSS Feed</h3>';`

Then call the PHP function or shortcode.

= I don't want the admin CSS. How do I remove it? =

Add this to your functions.php:

`remove_action('admin_head', 'insert_jpgrf_admin_css');`

== Screenshots ==

1. admin screen

== Changelog ==

= 1.3.9 =
- minor code refactoring
- added shortcode defaults display on settings page

= 1.3.8 =
- added donate link on admin page
- admin page CSS added
- admin page tweaks
- addressed issue from http://wordpress.org/support/topic/get-cannot-modify-header-information-error-message (I think)
- various sanitization and HTML5 options
- minor code refactoring

= 1.3.7 =
- minor code refactoring
- added option to open links in new window

= 1.3.6 =
- another minor admin page update

= 1.3.5 =
- minor admin page update

= 1.3.4 =
- updated admin messages code
- updated readme
- added feed item description option

= 1.3.3 =
* added quicktag to post editor toolbar button
* code refactoring

= 1.3.2 =
* correct handling of defaults
* fixed readme.txt errors

= 1.3.1 =
* added admin menu, moved settings there
* localized the plugin to prep for translation files (anyone want to contribute?)
* code refactoring

= 1.3.0 =
added shortcode, CSS styling, nofollow option, some code cleanup

= 1.2 =
January 2nd, 2012. Validated compatibility with WP 3.3. Added Genesis hook example to FAQ.

= 1.1 =
* Version 1.1 completed December 21st, 2010. This update fixes a bug when activating and using plugin.

= 1.0 =
* Version 1.0 completed December 28th, 2009.

== Upgrade Notice ==

= 1.3.9 =
- minor code refactoring
- added shortcode defaults display on settings page

= 1.3.8 =
- added donate link on admin page
- admin page CSS added
- admin page tweaks
- addressed issue from http://wordpress.org/support/topic/get-cannot-modify-header-information-error-message (I think)
- various sanitization and HTML5 options
- minor code refactoring

= 1.3.7 =
- minor code refactoring
- added option to open links in new window

= 1.3.6 =
- another minor admin page update

= 1.3.5 =
- minor admin page update

= 1.3.4 =
- updated admin messages code
- updated readme
- added feed item description option

= 1.3.3 =
* added quicktag to post editor toolbar button
* code refactoring

= 1.3.2 =
* correct handling of defaults
* fixed readme.txt errors

= 1.3.1 =
* added admin menu, moved settings there
* localized the plugin to prep for translation files (anyone want to contribute?)
* code refactoring

= 1.3.0 =
This version adds a shortcode, optional CSS styling, optional nofollow