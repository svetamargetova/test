<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'edventure_cz');

/** MySQL database username */
define('DB_USER', 'edventure_cz');

/** MySQL database password */
define('DB_PASSWORD', 'WU5hS');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',4{UudEU:Jv4M$xkx61(vHS(Q^nP=D2*0VPg[%O:+)d7u#_},X<M8cQY+j3lab8J');
define('SECURE_AUTH_KEY',  ',(o93{2X(Gl ksnj&;N1s!bD3MwkCl&p+8wQ|/B({7-AyHS&9$e)UWUvk/Q7->$U');
define('LOGGED_IN_KEY',    'SG+qq<=(D}[t9/57YsoG|3f4DTn=fde&(-8# *+&x4HM.fXYz& 5sj {QA!]qWG{');
define('NONCE_KEY',        'j-&3p<5#</7z%e- g w7W6*buYc Nx9^CM17s]znQ1b-}o}+ujh`kAYe>5@.kakh');
define('AUTH_SALT',        'E=)?/q%[3`M)Um/UYnN`E#YTqIud(MBQajVB6I7QGGfI1`n2Aiv!u?q2=? ,&?eQ');
define('SECURE_AUTH_SALT', 'p{B|+U$!m#($5]+eT}$K4Y!KHd;)>d.R+PGN)/>;[qKHYD66a{Ad~0!:V+N^V?W-');
define('LOGGED_IN_SALT',   'P5w)RRIWxe]r)p*0IStfYE8]}#L2t:089KF=ZJ=,Hms%?-7dG6L@bU*V^}mmH@g]');
define('NONCE_SALT',       'L+dklu/&Q1n&8Ua-n1]0]Q+tMe-3Y-dYK8*)SDV`2g>Z5}wHio.Cv %&jFX_q/2g');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', 'cs_CZ');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
