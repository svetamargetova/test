$(document).ready(function (){
	
	//Gallery
	$(".gallery a").viewbox();
	
	//Music - mp3
	$(".player a").viewbox();
	
	//YouTube films
	$(".video a").viewbox({
		widthWindow: 900 
		,
		navigation:0
	});
	
	//Vimeo films
	$(".vimeo a").viewbox({
		widthWindow: 900
	});
	
	//Frame 
	$(".other #frame").viewbox({
		frame: 1,
		heightWindow: 600,
		widthWindow: 900,
		navigation:0
	});
	
	//Inline
	$("#inlinelink").viewbox({
		heightWindow: 190,
		widthWindow: 450
	});
	
	//Ajax
	$("#ajax").viewbox({
		heightWindow:450,
		widthWindow: 555,
		ajaxSuccess: function(data) { 
			$(".vb_wrap .content .content").html(data);
		}
	});
	
	//Maps
	$("#map").viewbox({
		heightWindow: 600,
		widthWindow: 850
	})
}); 
