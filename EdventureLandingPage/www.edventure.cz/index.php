<?php 

 session_start();
 ob_start();
include "include/databaze.php";
include "include/PHPMailer_5.2.0/class.phpmailer.php";

function chyba($dotaz)
{
			$current_url = "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];  // zjištění aktualni url
			echo "Někde se stala chyba,\n <br> omlouváme se za problémy, už jsme povolali četu záložních programátorů na rychlé vyřešení problému".MySQL_Error()."";
		
 
	Die();
}
 	

?>
<!DOCTYPE html >
<html lang="cs">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Edventure</title> 
<meta name="description" content="Vzdělávejte se po svém">
<meta name="author" content="Grafika: Adam Amran - www.amran.cz, Kód: Tomáš Vojta - www.marshi.cz">
<meta name="robots" content="all">
<link rel="image_src" href="http://www.edventure.cz/images/logo.png" />
<meta property="og:image" content="http://www.edventure.cz/images/logo.png" />


<script src="include/jquery-latest.js" type="text/javascript"></script>
<script src="include/script.js" type="text/javascript"></script>

<script src="./js/cufon-yui.js" type="text/javascript"></script>
<script src="./js/BreeSerif_400.font.js" type="text/javascript"></script>
<script type="text/javascript">
  Cufon.replace('h1')('h2');
</script>

<script type="text/javascript" src="js/script.js"></script>	
<script type="text/javascript" src="js/viewbox_min.js"></script>

<script type="text/javascript" src="https://apis.google.com/js/plusone.js">
  {lang: 'cs'}
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36287374-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<link rel="stylesheet" type="text/css" href="viewbox-css.css" /> 
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
<link rel="stylesheet" href="style.css" type="text/css" media="screen" />
 
 
</head>
<body>

<div id="top">
      <a href="http://www.edventure.cz/"><img src="images/logo.png" alt="Logo Edunomy"></a>
</div>

<div id="obsah">
	<div id="modra-lista">
            
      <?php
			  
				if (!empty($_POST["email"]) )
				{
	 
				$email=$_POST['email'];
				$datum = Date("d.m.Y, H:i", Time());
  //echo "INSERT INTO `emaily` (`email`)  VALUES ('$email')";
				$uprava = MySQL_Query("INSERT INTO `emaily` (`email`, `cas`)  VALUES ('$email', '$datum')") or chyba(); 
				 
		$mail = new PHPMailer();
      $mail->IsSMTP();  // k odeslání e-mailu použijeme SMTP server
      $mail->From ="info@edventure.cz";   // adresa odesílatele skriptu
      $mail->FromName ="Edventure" ; // jméno odesílatele skriptu (zobrazí se vedle adresy odesílatele)
      $mail->AddAddress("info@edventure.cz");  
      $mail->Subject = "Nový zájemce";    // nastavíme předmět e-mailu
	 // $mail->AddAttachment("downloads/edunomy-manifest.pdf");
	  $mail->IsHTML(true);
	 	  $mail->Body =  "
Ahoj,
<br>
<br>

email ".$email." chce vědět jako první o spuštění projektu<br>
<br>
Tomáš
 ";
 
      $mail->CharSet = "utf-8";   // nastavíme kódování, ve kterém odesíláme e-mail

       if(!$mail->Send()) {  // odešleme e-mail
 
		 echo "Mail send error!";
      } 
		?>	
			 <h1>DĚKUJEME!</h1>	
		 <p class="modra">Těší nás váš zájem o Edventure.<br><br>

Váš email jsme si uložili, o spuštění projektu budete vědět jako první </p>	
         
   <!--      <a href="downloads/edunomy-manifest.pdf" target="_blank">Stáhnout manifest</a>	 -->
<?		
				}
				else {
				?>  
                 <h1><!--VÁŠ MOZEK SE KONEČNĚ NASYTÍ ---  DOBRODRUŽNÁ CESTA ZA VZDĚLÁNÍM ---VYDEJTE SE NA DOBRODRUŽSTVÍ Z OBJEVOVÁNÍ  --- prosekejte se vzdělávací džunglí -->VZDĚLÁVEJTE SE PO SVÉM</h1>
            <form method="post" action="" > <p class="modra"><label for="email">Zadejte svůj email a dozvíte se jako první o spuštění tohoto projektu<!-- + získáte náš manifest v PDF --></label>  </p>
            <input type="email" placeholder="Váš email..." required="required" id="email" name="email">
             <input type="submit" value="Chci být u toho">
             </form><?  }
				?> 
 	</div>        
<h2>Proč to děláme?</h2>
<p class="modra inspirace">Trocha projektové inspirace...</p>
<div class="hide_loader">
    <img src="images/mitrahover.png">
    <img src="images/steflhover.png">
    <img src="images/kenhover.png">
</div>
<table>
	<tr>
      <td colspan="2">
        	<div class="video">
            <a href="http://embed.ted.com/talks/lang/cs/sugata_mitra_the_child_driven_education.html" id="frame" title="" >
             <div class="mitra"></div>
            </a>

 
</div>
        </td>
    	<td colspan="2">
        	<div class="video">
<a href="http://www.youtube.com/watch?v=zDZFcDGpL4U" title="" ><div class="ken"></div></a>

</div>
        </td>
      
        <td colspan="2">
        	<div class="video">
<a href="http://www.youtube.com/watch?v=jt8mAdg-PIM" title="" ><div class="stefl"></div></a>

</div>
        </td>
    </tr>
    <tr>
    	
        <td ><p> <strong>Sugata Mitra</strong>: The child-driven education </p></td>
        <td><img src="images/czsub.png" alt="en"  class="video_info"title="Toto video je anglicky s českými titulky">
       </td>
        <td> <p><strong>Ken Robinson</strong>: RSA Animate  
Changing Education Paradigms</p></td>
        <td><img src="images/en.png" alt="en"  class="video_info"title="Toto video je pouze anglicky"></td>
        <td> <p><strong>Ondrej Šteffl</strong>: Je škola budoucnost vzdělávání?</p></td>
        <td><img src="images/cz.png" alt="en"  class="video_info"title="Toto video je česky"></td>
    </tr>
</table>
 
<table>
<tr><td width="34" valign="top"><div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/cs_CZ/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="fb-like"  data-send="false" data-href="http://www.edventure.cz"  data-layout="box_count" data-width="20" data-show-faces="true" data-font="arial"></div></td>
<td width="78" valign="top"><a href="https://twitter.com/share" class="twitter-share-button"    data-count="vertical" >Tweet</a><script type="text/javascript" src="//platform.twitter.com/widgets.js"></script></td>
<td width="5" valign="top"><g:plusone size="tall"  href="http://www.edventure.cz" ></g:plusone></td>
            </tr>
        </table>

 <img src="images/line.png" class="line">
			
       
</div>  
<div class="paticka ">   &copy; 2013 Edventure | &#105;&#110;&#102;&#111;&#64&#101;&#100;&#118;&#101;&#110;&#116;&#117;&#114;&#101;&#46;&#99;&#122;</div>
</body>
</html>
